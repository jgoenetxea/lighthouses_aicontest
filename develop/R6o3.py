#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import collections
import random

import interface

# LH = LightHouse


class Plan:
    def __init__(self):
        self.path = []  # This stores the path to the next target
        self.lh_list = []  # This stores the order in the LH triangle configuration


class R6o3(interface.Bot):
    """Bot que juega aleatoriamente."""
    NAME = "R6o3"

    def __init__(self, init_state):
        # This block is from the original constructor
        """Inicializar el bot: llamado al comienzo del juego."""
        self.player_num = init_state["player_num"]
        self.player_count = init_state["player_count"]
        self.init_pos = init_state["position"]
        self.map = init_state["map"]
        self.lighthouses = map(tuple, init_state["lighthouses"])

        # Custom constructor elements
        self.plan_list = []
        # TODO: Check not accessible lh instances

    def get_nearest_lh(self):
        pass

    def get_higher_close_cell(self):
        pass

    def check_plan(self, plan):
        return False

    def get_a_plan(self):
        pass

    def compute_path(self, source, target, map_grid):
        queue = collections.deque([[source]])
        seen = set([source])
        width = len(map_grid[0])
        height = len(map_grid)
        wall = 0
        # check if the source location is correct
        if map_grid[source[1]][source[0]] == wall:
            print "Error with the path initial location"
            path = queue.popleft()
            return path
        # find the path
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            # if map_grid[x][y] == target:
            if x == target[0] and y == target[1]:
                return path
            for x2, y2 in ((x + 1, y), (x - 1, y),  # horizontal
                           (x, y + 1), (x, y - 1),  # vertical
                           (x - 1, y - 1), (x + 1, y + 1),  # down-up diagonal
                           (x - 1, y + 1), (x + 1, y - 1)):  # up-down diagonal
                if 0 <= x2 < width \
                        and 0 <= y2 < height \
                        and map_grid[y2][x2] != wall \
                        and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))


def play(self, state):
        """Jugar: llamado cada turno.
        Debe devolver una acción (jugada)."""
        # get interesting data
        cx, cy = state["position"]
        lighthouses = dict((tuple(lh["position"]), lh)
                            for lh in state["lighthouses"])

        # Is the plan possible?
        while len(self.plan_list) > 0:
            if not self.check_plan(self.plan_list[0]):
                # Remove the plan from the list
                self.plan_list = self.plan_list[:-1]

        # Check if we have no plan, get one
        if len(self.plan_list) == 0:
            self.plan_list.append(self.get_a_plan())

        # Follow the plan


        # Si estamos en un faro...
        if (cx, cy) in self.lighthouses:
            # Probabilidad 60%: conectar con faro remoto válido
            if lighthouses[(cx, cy)]["owner"] == self.player_num:
                if random.randrange(100) < 60:
                    possible_connections = []
                    for dest in self.lighthouses:
                        # No conectar con sigo mismo
                        # No conectar si no tenemos la clave
                        # No conectar si ya existe la conexión
                        # No conectar si no controlamos el destino
                        # Nota: no comprobamos si la conexión se cruza.
                        if (dest != (cx, cy) and
                            lighthouses[dest]["have_key"] and
                            [cx, cy] not in lighthouses[dest]["connections"] and
                                lighthouses[dest]["owner"] == self.player_num):
                            possible_connections.append(dest)

                    if possible_connections:
                        return self.connect(random.choice(possible_connections))

            # Probabilidad 60%: recargar el faro
            if random.randrange(100) < 60:
                energy = random.randrange(state["energy"] + 1)
                return self.attack(energy)

        # Mover aleatoriamente
        moves = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
        # Determinar movimientos válidos
        moves = [(x, y) for x, y in moves if self.map[cy+y][cx+x]]
        move = random.choice(moves)
        return self.move(*move)

if __name__ == "__main__":
    iface = interface.Interface(R6o3)
    iface.run()
