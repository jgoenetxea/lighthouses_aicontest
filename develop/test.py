from R6o3 import R6o3


class Map_Data:
    def __init__(self):
        self.lighthouses = []
        self.players = []
        self.island = []


def read_map(mapfile):
    with open(mapfile, "r") as fd:
        lines = [l.replace("\n", "") for l in fd.readlines()]
    map_data = Map_Data()
    players = []
    for y, line in enumerate(lines[::-1]):
        row = []
        for x, c in enumerate(line):
            if c == "#":
                row.append(0)
            elif c == "!":
                row.append(1)
                map_data.lighthouses.append((x, y))
            elif c == " ":
                row.append(1)
            else:
                row.append(1)
                players.append((c, (x, y)))
        map_data.island.append(row)
    map_data.players = [pos for c, pos in sorted(players)]
    w = len(map_data.island[0])
    h = len(map_data.island)
    if not all(len(l) == w for l in map_data.island):
        print("All map rows must have the same width")
    if (not all(not i for i in map_data.island[0]) or
            not all(not i for i in map_data.island[-1]) or
            not all(not (i[0] or i[-1]) for i in map_data.island)):
        print("Map border must not be part of island")

    return map_data


def print_map(map_cells):
    print("Map cells:")
    for row in map_cells:
        for cell in row:
            print cell,
        print ""


def print_path(map_cells, path):
    tmp_map = map_cells[:]
    # define the different element values
    wall_label = 0
    space_label = 1
    start_label = 2
    end_label = 3
    path_label = 4
    # Format path elements
    start = path[0]
    tmp_map[start[1]][start[0]] = start_label
    for cell in path[1:-1]:
        tmp_map[cell[1]][cell[0]] = path_label
    end = path[-1]
    tmp_map[end[1]][end[0]] = end_label
    # Show the map
    print("Defined path:")
    for row in map_cells:
        for cell in row:
            if cell == wall_label:
                print "#",
            if cell == space_label:
                print " ",
            if cell == start_label:
                print "0",
            if cell == end_label:
                print "X",
            if cell == path_label:
                print "-",
        print ""


def test_path_finding(mapfile, source, target):
    # Read map
    map_data = read_map(mapfile)
    # Show the loaded map
    print_map(map_data.island)
    # Set initial status
    init_state = {}
    init_state["player_num"] = 1
    init_state["player_count"] = 1
    init_state["position"] = source
    init_state["map"] = map_data.island
    init_state["lighthouses"] = map_data.lighthouses
    # Init robot class
    bot = R6o3(init_state)

    path = bot.compute_path(source, target, map_data.island)
    if path is None:
        print "Error computing path"
    else:
        print_path(map_data.island, path)

if __name__ == "__main__":
    mapfile = "../maps/island.txt"
    # Define path elements
    source = (15, 16)
    target = (6, 1)
    test_path_finding(mapfile, source, target)
